# Vagrant LAMP Stack for Vagrant/Virtualbox

A pre-built LAMP stack using Vagrant/VirtualBox ( based on ScotchBox ).

## Getting Started

My primary purpose is to have this 'base box' available for me to clone/modify/configure for my own development projects.   

If you wish to start from scratch, I recommend going to...  
https://scotch.io/bar-talk/introducing-scotch-box-a-vagrant-lamp-stack-that-just-works  
...as the settings configured within this repo are customized for my own virtual environment.  

Nicholas Cerminara @ Scotch.io has done a great job of providing comprehensive installation & setup guidance.  
Links to resource/upstream provided down in the Acknowledgements.


### Prerequisites
Relies on:
* [Vagrant](https://www.vagrantup.com/) - Vagrant Utility
* [Virtualbox](https://www.virtualbox.org/) - Virtualization

### Installing

Again, this setup has been configured to my own individual settings.  
Yours will certainly vary based on a wide range of factors (i.e. host OS, drive/folder mappings, etc.)!

## Deployment

Clone to a new project, vagrant 'up', and away we go!

## Built With

* [Sublime Text 3](https://www.sublimetext.com/3) - Text Editor
* [Vagrant](https://www.vagrantup.com/) - Vagrant Utility
* [Virtualbox](https://www.virtualbox.org/) - Virtualization

## License

This project is licensed under the MIT License - see the [LICENSE.md](LICENSE.md) file for details

## Acknowledgments

Big thanks to Nicholas Cerminara @ Scotch.io, as his web page:  
https://scotch.io/bar-talk/introducing-scotch-box-a-vagrant-lamp-stack-that-just-works  
was the primary resource I used for creating this tool.

I've included Nicholas' original README.md in the file [README_ScotchBoxIO.md](README_ScotchBoxIO.md)
